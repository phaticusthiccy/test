var readline = require('readline');
var delay = ms => new Promise(resolve => setTimeout(resolve, ms))
const syncWait = ms => {
    const end = Date.now() + ms
    while (Date.now() < end) continue
}  
require('util').inspect.defaultOptions.depth = null
var api = require("instagram-web-api")

const rl = readline.createInterface({ input: process.stdin, output: process.stdout });
var info = { user: "", pass: "", time: 0}
rl.question("Kullanıcı Adı Gir >>> ", async (user) => {
    if (user !== "") info.user = user
    else throw new TypeError("Username Gir!")
    rl.question("Şifre Gir >>> ", async (pass) => {
        info.pass = pass
        rl.question("Kaç Saniyede Bir Unf Ypasın? >>> ", async(time) => {
            info.time = Number(time) * 1000
            const login = new api({ username: info.user, password: info.pass })
            try {
                await login.login()
            } catch (e) {
                throw new Error(e)
            }
            setInterval(async () => {
                const userdata = await login.getUserByUsername({ username: info.user });
                var myuserid = userdata['id'];
                let myFollowers = []

                let myId = myuserid

                let followers = await login.getFollowers({ userId: myId })
                
                var unf = followers.data[0].id
                await login.unfollow({ userId: unf })
            }, info.time)
        })
    })
})